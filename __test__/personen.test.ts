import request from "supertest";
// te testen app
import {app} from "../app";
import {describe, expect, test} from "vitest"

//  or
// let externalRequest = request ("http://localhost:3000");
let server = request(app);

describe("Get lijst van personen", () => {
    test("async", async () => {
        const res = await server.get("/api/persons");
        expect(res.statusCode).toBe(200);
        expect(res.body.length).toBe(3);
    });


    test("promise", () => {
        return server.get("/api/persons")
            .then(res => {
                expect(res.body.length).toBe(3);
                // @ts-ignore
                let karl = res.body.find((x) => x.id === 1)
                expect(karl).toBeDefined();
                expect(karl).toHaveProperty("name");
                expect(karl.name).toBe("karl");
            })
    });

    test("supertest .expect and promise", () => {
        return server.get("/api/persons")
            .expect(200)
            .then(res => expect(res.body.length).toBe(3))
    });


});
test("GET persoon met id", () => {
    let id = 2
    return server.get("/api/persons/" + id)
        .expect(200)
        .then(res => {
            expect(res.body.id).toBe(id)
        })
})

test("update persoon promises", () => {
    let url = "/api/persons/3";
    return server.put(url)
        .send({id: 3, name: "groucho", birthdate: "01/04/1987"})
        .then(res => {
            expect(res.statusCode).toBe(200);
            return request(app).get("/api/persons/3");
        })
        .then(res => {
            expect(res.body).toEqual({
                id: 3,
                name: "groucho",
                birthdate: "01/04/1987"
            });
        })
});

// running this test with jest results in warning:
// Jest did not exit one second after the test run has completed.
describe("Add person" ,() => {
    test("urlencode supertest/Foute test: altijd OK", () => {
        server.post("/api/persons")
            .send("name=chico&birthdate=22/03/1887")
            .expect(666)  // test ongeldige status code
            .expect("Location", /api\/persons\/*[0-9]+/)
    });


    test(" json persoon met promise", () => {
        return request(app).post("/api/persons")
            .send({
                "name": "chico",
                "birthdate": "22/03/1887"
            })
            .then(res => {
                expect(res.statusCode).toBe(201);
                expect(res.get("Location")).toMatch(/\/api\/persons\/*[0-9]+$/);
            })
    });
})




