import express, {Request, Response} from "express";
import {router as jsonRouter} from "./route/restRouter";

export const app= express();

app.use("/api/persons",jsonRouter);
app.use(express.static("../public"));

app.get("/about",(req:Request,resp:Response) => resp.send("data server demo"));
