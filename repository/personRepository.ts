export interface Person{
    id:number,
    name:string,
    birthdate: string
}

export const persons:Person[] = [
    {id: 1, name: "karl", birthdate: "5/5/1818"},
    {id: 2, name: "ingeborg", birthdate: "24/4/1970"},
    {id: 3, name: "groucho", birthdate: "2/10/1890"}
];
