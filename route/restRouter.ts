import express, {Request, Response} from "express";
import {persons} from "../repository/personRepository"


export const router = express.Router();

router.use(express.json())
router.use(express.urlencoded({ extended: true }));

let nextKey = Math.max.apply(Math, persons.map(p => p.id)) + 1


router.get("/", getPersons);
router.get("/:id", (req: Request, resp: Response) => resp.json(persons.find(p => p.id === +req.params.id)));
router.put("/:id", changePerson);

router.delete("/:id", deletePerson);
router.post("/", addPerson);
function changePerson(req: Request, resp: Response) {
    // + is used to convert string to number
    const idx = persons.findIndex(p => p.id === +req.params.id);
    req.body.id = +req.body.id;
    if (idx >= 0) {
        persons[idx] = req.body;
        resp.status(200);
    } else {
        persons.push(req.body)
        resp.status(201);
        if (req.body.id >= nextKey) {
            nextKey = req.body.id + 1;
        }
    }
    resp.send();
}

function addPerson(req: Request, resp: Response) {
    req.body.id = nextKey
    persons.push(req.body);
    resp.status(201)
        .location(`${req.originalUrl}/${nextKey}`)
        .send();
    nextKey++;
}

function getPersons(req: Request, resp: Response) {
    const name = req.query.name
    const found = name
        ? persons.filter(p => p.name === name)  // if name present filter
        : persons // else no query: return all
    if (found.length > 0) resp.json(found)
    else resp.sendStatus(404)
}

function deletePerson(req: Request, resp: Response) {
    const idx = persons.findIndex(p => p.id === +req.params.id);
    if (idx >= 0) {
        persons.splice(idx, 1)
        resp.sendStatus(204);
    } else {
        resp.sendStatus(404)
    }
}
